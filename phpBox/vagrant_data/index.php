<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Test serveur</title>
  </head>
  <body>
    <h1>Test de fonctions php sur serveur vagrant</h1>
    <h2>Opérations de fichiers</h2>
    <?php echo "<pre>"; print_r(scandir("./")); echo "</pre>"; ?>
    <a href="index.php?clear">Clear</a><br>
    <textarea name="name" rows="50" cols="100"><?=checkAndWriteFile("data.txt", "Hello world !!!\r\n");?></textarea>
  </body>
</html>


<?php
if(isset($_GET['clear'])){
  checkAndWriteFile("data.txt", "", true);
  header('Location: ./');
}

function checkAndWriteFile($name, $content, $clear = false){
  if($clear){
    file_put_contents($name, $content);
  }else{
    if(file_exists($name) && is_readable($name) && is_writable($name)){
      if(is_dir($name)){
        return "file is a directory, aborting";
      }else{
        file_put_contents($name, $content, FILE_APPEND);
      }
    }else{
      file_put_contents($name, $content);
    }
  }
  return file_get_contents($name);
}
?>
