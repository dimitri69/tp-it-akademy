<?php
session_start();
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Générer vos sites internet simplement et facilement">
    <meta name="author" content="Dimitri Sandron">
    <link rel="icon" href="./img/industrial-robot.png">
    <title>Générateur de site internet</title>
    <link rel="stylesheet" href="./css/style.css" media="screen">
    <link rel="stylesheet" href="./css/style_site.css" media="screen">
  </head>
  <body>
    <header>
      <div class="icon">
        <a href="./"><img src="./img/industrial-robot.png" alt="" id="img_logo"/></a>
        <img src="https://dummyimage.com/300x100/78909c/000.png&text=Générateur de site en ligne" alt="Logo site" />
      </div>
      <nav class="navbar navbar-inverse navbar-fixed-top">
        <ul>
          <li><a class="btn" href="./">Accueil</a></li>
          <li><a class="btn" href="./">Contact</a></li>
        </ul>
      </nav>
      <?php if(!empty($_SESSION['result'])): ?>
        <div class="user_info">
          <span class="user_name"><?=(!empty($_SESSION['result']['prenom']))?$_SESSION['result']['prenom']:''?> <?=(!empty($_SESSION['result']['nom']))?$_SESSION['result']['nom']:''?></span><br><br>
          <a class="btn" href="./result.php">Mon site</a>
          <a class="btn" href="./action.php?reset=1">Reset</a>
        </div>
      <?php endif; ?>
    </header>
