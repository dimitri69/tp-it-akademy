<?php

define('THEME_FOOT', 'football');
define('THEME_CUISINE', 'cuisine');

session_start();

/*echo "<pre>"; var_dump($_POST); echo "</pre>";
echo "<pre>"; var_dump($_SESSION); echo "</pre>";*/

if(!empty($_POST['launch'])){

  // On enresgitre en session tous les champs du formulaire
  foreach ($_POST as $champs => $value) {
    $_SESSION['result'][$champs] =  $value;
  }

  // Traitement du thême
  if(!empty($_POST['theme_site'])){
    switch($_POST['theme_site']){
      case THEME_FOOT:
        $_SESSION['theme'] = 'football_theme';
        break;
      case THEME_CUISINE:
        $_SESSION['theme'] = 'cuisine_theme';
        break;
    }
  }

  // Uplaod de l'image principale
  $uploaddir = './uploads/';
  $uploadfile = $uploaddir . basename($_FILES['image_site']['name']);
  if (!move_uploaded_file($_FILES['image_site']['tmp_name'], $uploadfile)) {
      //echo "Erreur upload fichier\n";
  }else{
    $_SESSION['result']['image_site'] = $_FILES['image_site']['name'];
  }

  // Traitement du pseudocode
  $patterns = array();
  $patterns[0] = "/([#]{2})([a-zA-Z0-9\,\!\'\.\s]*)([#]{2})/";  // ##Mon titre
  $patterns[1] = "/([&]{2})([a-zA-Z0-9\,\!\'\.\s]*)([&]{2})/";  // ###Mon sous-titre
  $patterns[2] = "/([*]{2})([a-zA-Z0-9\,\!\'\.\s]*)([*]{2})/";  // **Mon texte en gras**
  $patterns[3] = "/([-]{2})([a-zA-Z0-9\,\!\'\.\s]*)([-]{2})/";  // **Mon texte en gras**
  $replacements = array();
  $replacements[0] = "<h2>$2</h2>";
  $replacements[1] = "<h3>$2</h3>";
  $replacements[2] = "<strong>$2</strong>";
  $replacements[3] = "<u>$2</u>";
  $_SESSION['pseudocode']['pseudocode_central'] = preg_replace($patterns, $replacements, $_POST['pseudocode_central']);
  $_SESSION['pseudocode']['pseudocode_aside'] = preg_replace($patterns, $replacements, $_POST['pseudocode_aside']);

  $patterns = '/([#]{2})([a-zA-Z0-9\'\.\s]*)([#]{2})/';
  $replacements = '<h1>$0</h1>';

  header('Location: ./result.php ');

} else if(!empty($_GET['reset'])){
  $_SESSION = array();
  session_destroy();
  header('Location: ./ ');
}



?>
