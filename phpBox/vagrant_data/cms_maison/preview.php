<?php
session_start();
include_once('functions.php');
include_once('env.php');
$page_id = $_GET['page_id'];
$page = getPageDetails($page_id);
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Preview de la page</title>
    <link rel="stylesheet" href="./css/style.css" media="screen" title="no title">
    <link rel="stylesheet" href="./generated_pages/css/<?=$page['theme_site']?>.css" media="screen" title="no title">
  </head>
  <body>
    <header>
      <div class="icon">
        <a href="./"><img src="./img/industrial-robot.png" alt="" id="img_logo"/></a>
        <img src="https://dummyimage.com/300x100/78909c/000.png&text=Générateur de site en ligne" alt="Logo site" />
      </div>
      <nav class="navbar navbar-inverse navbar-fixed-top">
        <ul>
          <li><a class="btn" href="./index.php">Accueil</a></li>
          <li><a class="btn" href="./new_page.php">Nouvelle page</a></li>
        </ul>
      </nav>
      <?php if(!empty($_SESSION['result'])): ?>
        <div class="user_info">
          <span class="user_name"><?=(!empty($_SESSION['result']['prenom']))?$_SESSION['result']['prenom']:''?> <?=(!empty($_SESSION['result']['nom']))?$_SESSION['result']['nom']:''?></span><br><br>
          <a class="btn" href="./result.php">Mon site</a>
          <a class="btn" href="./action.php?reset=1">Reset</a>
        </div>
      <?php endif; ?>
    </header>

    <main>
      <h1>Visualisation page : Page <?=$page_id?></h1>
      <?php //debug(showPreview("./generated_pages/HTML/index_".$page_id.".html")); ?>
      <?= showPreview("./generated_pages/HTML/index_".$page_id.".html"); ?>
    </main>

  </body>
</html>
