<?php
session_start();
include_once('functions.php');
include_once('env.php');
$page_id = lastPage();
$page = getPageDetails($page_id);
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title><?=(!empty($page['nom_site']))?$page['nom_site']:''?></title>
    <?php if(!empty($page['theme_site'])): ?>
      <link rel="stylesheet" href="./css/<?=$page['theme_site'].".css"?>" media="screen" title="no title">
    <?php endif; ?>
  </head>
  <body>
<?php
session_start();
include_once('functions.php');
include_once('env.php');
$page_id = lastPage();
$page = getPageDetails($page_id);
?>

<footer class="">
  (c) <?=(!empty(<?=(!empty($page['copyright']))?$page['copyright']:''?> <?=date('Y')?>
</footer>
</div>
</main>
</div>
