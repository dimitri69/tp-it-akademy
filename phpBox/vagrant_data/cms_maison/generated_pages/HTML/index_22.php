<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title><?=(!empty($page['nom_site']))?$page['nom_site']:''?></title>
    <?php if(!empty($page['theme_site'])): ?>
      <link rel="stylesheet" href="./css/jaune'.css'?>" media="screen" title="no title">
    <?php endif; ?>
  </head>
  <body>
<?php
session_start();
include_once('functions.php');
include_once('env.php');
$page_id = lastPage();
$page = getPageDetails($page_id);
?>

<main>
  <div class="header_site">
    <div class="logo_site">
      <img src="./img/<?=(!empty($page['nom_site']))?$page['nom_site']:''?>" alt="Logo site généré" class="preview_site" />
    </div>
  </div>
  <div id="accueil" class="content">
    <section class="">
      <article class="">
        <h2 class="preview_site"><?=(!empty($page['titre']))?$page['titre']:''?></h2>
        <div class="float-right preview_site">
          <?=(!empty($page['contenu_central']))?$page['contenu_central']:''?>
        </div>
        <div class="clear"></div>
      </article>
    </section>

    <aside class="">
        <?=(!empty($page['contenu_aside']))?$page['contenu_aside']:''?>
    </aside>
  </div>
</main>
<?php
session_start();
include_once('functions.php');
include_once('env.php');
$page_id = lastPage();
$page = getPageDetails($page_id);
?>


<footer class="">
  (c) <?=(!empty(<?=(!empty($page['copyright']))?$page['copyright']:''?> <?=date('Y')?>
</footer>
</body>
</html>
