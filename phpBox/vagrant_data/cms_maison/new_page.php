<?php
session_start();
include_once('functions.php');
include_once('env.php');
if(!empty($_GET['page_id'])){
  $page_id = $_GET['page_id'];
}else{
  $page_id = lastPage();
}
$page = getPageDetails($page_id);
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Mes pages</title>
    <link rel="stylesheet" href="./css/style.css" media="screen" title="no title">
  </head>
  <body>
    <header>
      <div class="icon">
        <a href="./"><img src="./img/industrial-robot.png" alt="" id="img_logo"/></a>
        <img src="https://dummyimage.com/300x100/78909c/000.png&text=Générateur de site en ligne" alt="Logo site" />
      </div>
      <nav class="navbar navbar-inverse navbar-fixed-top">
        <ul>
          <li><a class="btn" href="./index.php">Accueil</a></li>
          <li><a class="btn" href="./new_page.php">Nouvelle page</a></li>
        </ul>
      </nav>
      <?php if(!empty($_SESSION['result'])): ?>
        <div class="user_info">
          <span class="user_name"><?=(!empty($_SESSION['result']['prenom']))?$_SESSION['result']['prenom']:''?> <?=(!empty($_SESSION['result']['nom']))?$_SESSION['result']['nom']:''?></span><br><br>
          <a class="btn" href="./result.php">Mon site</a>
          <a class="btn" href="./action.php?reset=1">Reset</a>
        </div>
      <?php endif; ?>
    </header>

    <main>
      <h1>Nouvelle page : Page <?=$page_id?></h1>
      <form class="" action="./traitement.php" method="post" enctype="multipart/form-data">
    	  <fieldset>
    	    <legend>A propos de votre futur site</legend>
    			<p>
    				<label for="nom_site">Nom du site</label>
    		    <input type="text" name="nom_site" id="nom_site" value="<?=(!empty($page['nom_site']))?$page['nom_site']:''?>">
    			</p>
          <p>
    				<label for="copyright">Copyright (Votre nom)</label>
    		    <input type="text" name="copyright" id="copyright" value="<?=(!empty($page['copyright']))?$page['copyright']:''?>">
    			</p>
    			<p>
    				<label for="theme_site">Thême du site</label>
            <table class="choix_theme_site">
      	      <tr>
      					<td rowspan="2">Thême de site</td>
      	        <td><label for="bleu"><div class="preview_theme_bleu"></div></label></td>
      	        <td><label for="jaune"><div class="preview_theme_jaune"></div></label></td>
      	      </tr>
      	      <tr>
      	        <td>Thême bleu<br><input type="radio" name="theme_site" value="bleu" id="bleu" <?=(!empty($page['vitrine']))?'checked':''?>></td>
      	        <td>Thême jaune<br><input type="radio" name="theme_site" value="jaune" id="jaune" <?=(!empty($page['ecommerce']))?'checked':''?>></td>
      	      </tr>
      	    </table>
    			</p>
    			<table class="choix_type_site">
    	      <tr>
    					<td rowspan="2">Type de site</td>
    	        <td><label for="vitrine"><img src="img/ecommerce-1705786_640.png" alt="Site vitrine" /></label></td>
    	        <td><label for="ecommerce"><img src="img/web-site-1706400_640.png" alt="Site e-commerce" /></label></td>
    	      </tr>
    	      <tr>
    	        <td>Site vitrine<br><input type="radio" name="type_site" value="vitrine" id="vitrine" <?=(!empty($page['vitrine']))?'checked':''?>></td>
    	        <td>Site e-commerce<br><input type="radio" name="type_site" value="ecommerce" id="ecommerce" <?=(!empty($page['ecommerce']))?'checked':''?>></td>
    	      </tr>
    	    </table>
    	  </fieldset>
    	  <fieldset>
    	    <legend>Contenu de votre futur site</legend>
    			<p>
    				<label for="logo_site">Image principale de votre site</label>
    				<input type="file" name="logo_site" id="logo_site" value="<?=(!empty($page['logo_site']))?$page['logo_site']:''?>">
    			</p>
    			<p>
    				<div class="pseudocode_header_bloc">
    					<label for="pseudocode_central">Contenu centrale de votre site (en pseudo-code)</label> <a href="" class="pseudo_code">(Plus d'infos sur le pseudo-code)</a>
    					<div class="help_pseudocode">
    						<p>
    							<strong>Titres</strong><br>
    							##Titre en gras##<br>
    							&&Sous titres&&
    						</p>
    						<p>
    							<strong>Formattage texte</strong><br>
    							**Texte en gras**<br>
    							--Texte souligné--
    						</p>
                <p>
                  Faire un ajout de loremIpsum rapide
                </p>
    					</div>
    				</div>
    				<textarea name="contenu_central" id="contenu_central" rows="20" cols="40"><?=(!empty($page['contenu_central']))?$page['contenu_central']:''?></textarea>
    			</p>
    			<p>
    				<div class="pseudocode_header_bloc">
    					<label for="pseudocode_aside">Contenu barre latérale de votre site (en pseudo-code)</label> <a href="" class="pseudo_code">(Plus d'infos sur le pseudo-code)</a>
    					<div class="help_pseudocode">
    						<p>
    							<strong>Titres</strong><br>
    							##Titre en gras##<br>
    							&&Sous titres&&
    						</p>
    						<p>
    							<strong>Formattage texte</strong><br>
    							**Texte en gras**<br>
    							--Texte souligné--
    						</p>
    					</div>
    				</div>
    				<textarea name="contenu_aside" id="contenu_aside" rows="20" cols="40"><?=(!empty($page['contenu_aside']))?$page['contenu_aside']:''?></textarea>
    			</p>
    	  </fieldset>
    	  <fieldset>
    	    <legend>Confirmation</legend>
    	    <input type="checkbox" name="accept" id="accept" <?=(isset($page['accept']))?'checked':''?>>
    	    <label for="accept">Je confirme avoir pris connaissance et patati et patata...</label><br><br>
    	    <input type="submit" name="launch" value="Valider">
    	  </fieldset>
    	</form>
    </main>

  </body>
</html>
