<main>
  <div class="header_site">
    <div class="logo_site">
      <img src="./img/{{nom_site}}" alt="Logo site généré" class="preview_site" />
    </div>
  </div>
  <div id="accueil" class="content">
    <section class="">
      <article class="">
        <h2 class="preview_site">{{titre}}</h2>
        <div class="preview_site">
          {{contenu_central}}
        </div>
        <div class="clear"></div>
      </article>
    </section>

    <aside class="">
        {{contenu_aside}}
    </aside>
  </div>
</main>
