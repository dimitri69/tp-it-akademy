<?php
session_start();
include_once('functions.php');
include_once('env.php');
$pages = getAllPages();
?>
<?php //debug(getAllPages()); ?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>CMS Maison</title>
    <link rel="stylesheet" href="./css/style.css" media="screen" title="no title">
    <link rel="stylesheet" href="./css/card.css" media="screen" title="no title">
  </head>
  <body>

    <header>
      <div class="icon">
        <a href="./"><img src="./img/industrial-robot.png" alt="" id="img_logo"/></a>
        <img src="https://dummyimage.com/300x100/78909c/000.png&text=Générateur de site en ligne" alt="Logo site" />
      </div>
      <nav class="navbar navbar-inverse navbar-fixed-top">
        <ul>
          <li><a class="btn" href="./index.php">Accueil</a></li>
          <li><a class="btn" href="./new_page.php">Nouvelle page</a></li>
        </ul>
      </nav>
      <?php if(!empty($_SESSION['result'])): ?>
        <div class="user_info">
          <span class="user_name"><?=(!empty($_SESSION['result']['prenom']))?$_SESSION['result']['prenom']:''?> <?=(!empty($_SESSION['result']['nom']))?$_SESSION['result']['nom']:''?></span><br><br>
          <a class="btn" href="./result.php">Mon site</a>
          <a class="btn" href="./action.php?reset=1">Reset</a>
        </div>
      <?php endif; ?>
    </header>

    <main>
      <section>
        <h2>Mes pages générés</h2>
        <div class="">
          <ul class="list_pages">
          <?php foreach($pages as $page_id => $page): ?>
            <div class="wrapper">
          		<div class="card radius shadowDepth1">
          			<div class="card__image border-tlr-radius">
          				<img src="./generated_pages/img/<?=$page['logo_site']?>" alt="image" class="border-tlr-radius">
                </div>
          			<div class="card__content card__padding">
                  <div class="card__share">
                      <div class="card__social">
                          <a class="share-icon facebook" href="./preview.php?page_id=<?=$page_id?>"><span class="fa fa-eye"></span></a>
                          <a class="share-icon twitter" href="./new_page.php?page_id=<?=$page_id?>"><span class="fa fa-pencil"></span></a>
                          <a class="share-icon googleplus" href="./traitement.php?delete=oui&page_id=<?=$page_id?>"><span class="fa fa-trash"></span></a>
                      </div>
                      <a id="share" class="share-toggle share-icon" href="#"></a>
                  </div>
          				<div class="card__meta">
          					<a href="#">Page <?=$page_id?></a><time><?=$page['created']?></time>
          				</div>
          				<article class="card__article">
      	    				<h2><a href="#"><?=$page['nom_site']?></a></h2>
      	    				<p><?=(strlen(trim($page['contenu_central']))>30)?substr(trim($page['contenu_central']),0,45):$page['contenu_central']?></p>
      	    			</article>
          			</div>
          			<div class="card__action">
          				<div class="card__author">
          					<div class="card__author-content">
          						By <?=$page['copyright']?>
          					</div>
          				</div>
          			</div>
          		</div>
          	</div>
          <?php endforeach; ?>
            <div class="wrapper">
          		<div class="card radius shadowDepth1">
          			<div class="card__image border-tlr-radius">
          				<a href="./new_page.php"><img src="https://dummyimage.com/300x450/78909c/000.png&text=Nouvelle page" alt="image" class="border-tlr-radius new_page"></a>
                </div>
          		</div>
          	</div>
          </ul>
        </div>
      </section>
    </main>
    <script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
    <script src="./js/card.js" charset="utf-8"></script>
  </body>
</html>
