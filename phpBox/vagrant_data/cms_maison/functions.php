<?php

function lastPage(){
  if(empty($_SESSION['pages'])){
    return 1;
  }
  $nb_pages = key( array_slice( $_SESSION['pages'], -1, 1, TRUE ) );
  return $nb_pages + 1;
}

function getAllPages(){
  if(!isset($_SESSION['pages'])){
    $_SESSION['pages'] = array();
  }
  return $_SESSION['pages'];
}

function getPageDetails($page_id){
  if(!empty($_SESSION['pages'][$page_id])){
    return $_SESSION['pages'][$page_id];
  }
  return false;
}

function debug($var){
  echo "<pre>";
  var_dump($var);
  echo "</pre>";
}

function getTemplate($tpl){
  if(file_exists($tpl) && is_readable($tpl) && is_writable($tpl)){
    return file_get_contents($tpl);
  }
  return false;
}

function generateFile($name, $content){
  if(file_exists($name) && is_readable($name) && is_writable($name)){
    if(is_dir($name)){
      return "file is a directory, aborting";
    }else{
      file_put_contents($name, $content, FILE_APPEND);
    }
  }else{
    file_put_contents($name, $content);
  }
  return file_get_contents($name);
}

function showPreview($generated_file){
  if(file_exists($generated_file) && is_readable($generated_file) && is_writable($generated_file)){
    return file_get_contents($generated_file);
  }
  return false;
}

?>
