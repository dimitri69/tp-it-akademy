<?php

include_once('functions.php');
include_once('env.php');

session_start();

if(!empty($_POST['launch'])){

  $page_id = lastPage();

  // On enresgitre en session tous les champs du formulaire
  foreach ($_POST as $champs => $value) {
    $_SESSION['pages'][$page_id][$champs] = $value;
  }

  // Uplaod du logo du site
  $uploaddir = './generated_pages/img/';
  $uploadfile = $uploaddir . basename($_FILES['logo_site']['name']);
  if (!move_uploaded_file($_FILES['logo_site']['tmp_name'], $uploadfile)) {
      //echo "Erreur upload fichier\n";
  }else{
    $_SESSION['pages'][$page_id]['logo_site'] = $_FILES['logo_site']['name'];
  }

  // Traitement du pseudocode
  $patterns = array();
  $patterns[0] = "/([#]{2})([a-zA-Z0-9\,\!\'\.\s]*)([#]{2})/";  // ##Mon titre
  $patterns[1] = "/([&]{2})([a-zA-Z0-9\,\!\'\.\s]*)([&]{2})/";  // ###Mon sous-titre
  $patterns[2] = "/([*]{2})([a-zA-Z0-9\,\!\'\.\s]*)([*]{2})/";  // **Mon texte en gras**
  $patterns[3] = "/([-]{2})([a-zA-Z0-9\,\!\'\.\s]*)([-]{2})/";  // --Mon texte souligné--
  $replacements = array();
  $replacements[0] = "<h2>$2</h2>";
  $replacements[1] = "<h3>$2</h3>";
  $replacements[2] = "<strong>$2</strong>";
  $replacements[3] = "<u>$2</u>";
  $_SESSION['pages'][$page_id]['contenu_central'] = preg_replace($patterns, $replacements, $_POST['contenu_central']);
  $_SESSION['pages'][$page_id]['contenu_aside'] = preg_replace($patterns, $replacements, $_POST['contenu_aside']);

  // Date de création
  $_SESSION['pages'][$page_id]['created'] = date('d M Y');

  ////////////////////////////////////////////////////////////////////////
  // Gestion des templates - Création du fichier HTML de la page
  ////////////////////////////////////////////////////////////////////////

  $header_tpl = "./tpl/headers/header1.php";  // Un seul header pour commencer
  $footer_tpl = "./tpl/footers/footer1.php";  // Un seul footer pour commencer

  // Traitement du type de site (pour connaitre le template du contenu)
  $container_tpl = "./tpl/containers/vitrine.php";
  if(!empty($_POST['type_site'])){
    switch($_POST['type_site']){
      case TYPE_SITE_VITRINE:
        $container_tpl = "./tpl/containers/vitrine.php";
        break;
      case TYPE_SITE_ECOMMERCE:
        $container_tpl = "./tpl/containers/ecommerce.php";
        break;
    }
  }

  $page_details = getPageDetails($page_id);
  $header = str_replace(array("{{theme_site}}","{{titre_site}}"), array($page_details['theme_site'],$page_details['titre_site']), getTemplate($header_tpl));
  $container = str_replace(array("{{nom_site}}","{{titre}}","{{contenu_central}}","{{contenu_aside}}"), array($page_details['nom_site'],$page_details['titre'],$page_details['contenu_central'],$page_details['contenu_aside']), getTemplate($container_tpl));
  $footer = str_replace(array("{{copyright}}","{{date}}"), array($page_details['copyright'],date('Y')), getTemplate($footer_tpl));

  // Création du fichier de la page HTML
  $name_generated_file = "./generated_pages/HTML/index_".$page_id.".html";
  generateFile($name_generated_file, $header);
  generateFile($name_generated_file, $container);
  debug(generateFile($name_generated_file, $footer));

  header('Location: ./preview.php?page_id='.$page_id.' ');

}

else if(!empty($_GET['reset'])){
  $_SESSION = array();
  session_destroy();
  header('Location: ./index.php ');
}

else if(!empty($_GET['delete'])){
  if(!empty($_GET['page_id'])){
    $page_id = $_GET['page_id'];
    unset($_SESSION['pages'][$page_id]);
  }
  header('Location: ./index.php ');
}

?>
