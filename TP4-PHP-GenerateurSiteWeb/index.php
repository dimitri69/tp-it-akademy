<?php include_once('./tpl/header.php') ?>
<div class="jumbotron">
	<div class="container">
		<h1>Hello, world!</h1>
		<p>Sur ce site, vous pouvez générer un site internet simple, en répondant à seulement quelques questions.</p>
		<p><a class="btn btn-primary btn-lg" href="#" role="button">Allons-y ! &raquo;</a></p>
	</div>
</div>
<div class="container">
	<h1>Générateur de site internet</h1>
	<h2>Répondez au questionnaire pour générer votre site</h2>
	<form class="" action="action.php" method="post" enctype="multipart/form-data">
	  <fieldset>
	    <legend>A propos de vous</legend>
			<p>
				<label for="nom">Votre nom</label>
				<input type="text" name="nom" id="nom" value="<?=(!empty($_SESSION['result']['nom']))?$_SESSION['result']['nom']:''?>" placeholder="Votre nom">
			</p>
			<p>
				<label for="prenom">Votre prénom</label>
		    <input type="text" name="prenom" id="prenom" value="<?=(!empty($_SESSION['result']['prenom']))?$_SESSION['result']['prenom']:''?>" placeholder="Votre prénom">
			</p>
			<!--<p>
				<label for="date_naissance">Votre date de naissance</label>
		    <input type="date" name="date_naissance" id="date_naissance" value="<?=(!empty($_SESSION['result']['date_naissance']))?$_SESSION['result']['date_naissance']:''?>">
			</p>-->
	  </fieldset>
	  <fieldset>
	    <legend>A propos de votre futur site</legend>
			<p>
				<label for="nom_site">Nom du site</label>
		    <input type="text" name="nom_site" id="nom_site" value="<?=(!empty($_SESSION['result']['nom_site']))?$_SESSION['result']['nom_site']:''?>">
			</p>
			<p>
				<label for="theme_site">Thême du site</label>
		    <select class="" name="theme_site" id="theme_site">
		      <option <?=(!empty($_SESSION['result']['theme_site']) && $_SESSION['result']['theme_site'] == 'football')?'selected':''?> value="football">Football</option>
		      <option <?=(!empty($_SESSION['result']['theme_site']) && $_SESSION['result']['theme_site'] == 'cuisine')?'selected':''?> value="cuisine">Cusine</option>
		    </select>
			</p>
			<!--<table class="choix_type_site">
	      <tr>
					<td rowspan="2">Type de site</td>
	        <td><label for="vitrine"><img src="img/ecommerce-1705786_640.png" alt="Site vitrine" /></label></td>
	        <td><label for="ecommerce"><img src="img/web-site-1706400_640.png" alt="Site e-commerce" /></label></td>
	      </tr>
	      <tr>
	        <td>Site vitrine<br><input type="radio" name="type_site" value="vitrine" id="vitrine" <?=(!empty($_SESSION['result']['vitrine']))?'checked':''?>></td>
	        <td>Site e-commerce<br><input type="radio" name="type_site" value="ecommerce" id="ecommerce" <?=(!empty($_SESSION['result']['ecommerce']))?'checked':''?>></td>
	      </tr>
	    </table>-->
	  </fieldset>
	  <fieldset>
	    <legend>Contenu de votre futur site</legend>
			<p>
				<label for="icon_site">Image principale de votre site</label>
				<input type="file" name="image_site" id="image_site" value="<?=(!empty($_SESSION['result']['image_site']))?$_SESSION['result']['image_site']:''?>">
			</p>
			<p>
				<div class="pseudocode_header_bloc">
					<label for="pseudocode_central">Contenu centrale de votre site (en pseudo-code)</label> <a href="" class="pseudo_code">(Plus d'infos sur le pseudo-code)</a>
					<div class="help_pseudocode">
						<p>
							<strong>Titres</strong><br>
							##Titre en gras<br>
							&&Sous titres&&
						</p>
						<p>
							<strong>Formattage texte</strong><br>
							**Texte en gras**<br>
							--Texte souligné--
						</p>
					</div>
				</div>
				<textarea name="pseudocode_central" id="pseudocode_central" rows="20" cols="40"><?=(!empty($_SESSION['result']['pseudocode_central']))?$_SESSION['result']['pseudocode_central']:''?></textarea>
			</p>
			<p>
				<div class="pseudocode_header_bloc">
					<label for="pseudocode_aside">Contenu barre latérale de votre site (en pseudo-code)</label> <a href="" class="pseudo_code">(Plus d'infos sur le pseudo-code)</a>
					<div class="help_pseudocode">
						<p>
							<strong>Titres</strong><br>
							##Titre en gras<br>
							&&Sous titres&&
						</p>
						<p>
							<strong>Formattage texte</strong><br>
							**Texte en gras**<br>
							--Texte souligné--
						</p>
					</div>
				</div>
				<textarea name="pseudocode_aside" id="pseudocode_aside" rows="20" cols="40"><?=(!empty($_SESSION['result']['pseudocode_aside']))?$_SESSION['result']['pseudocode_aside']:''?></textarea>
			</p>
	  </fieldset>
	  <fieldset>
	    <legend>Confirmation</legend>
	    <input type="checkbox" name="accept" id="accept" <?=(isset($_SESSION['result']['accept']))?'checked':''?>>
	    <label for="accept">Je confirme avoir pris connaissance et patati et patata...</label><br><br>
	    <input type="submit" name="launch" value="Valider">
	  </fieldset>
	</form>
</div>

<?php include_once('./tpl/footer.php') ?>
