<?php include_once('./tpl/header.php') ?>
<div class="container">
  <h1>Générateur de site internet</h1>
  <h2>Votre site internet généré</h2>

  <main>
    <h3>Prévisualisation de votre site</h3>
    <div class="preview_bloc">
      <div class="preview_site header_site">
        <div class="preview_site logo_site">
          <img src="https://dummyimage.com/200x100/000000/fff.png&text=<?=(!empty($_SESSION['result']['nom_site']))?urlencode($_SESSION['result']['nom_site']):''?>" alt="Logo site généré" class="preview_site" />
        </div>
      </div>
      <div id="accueil" class="preview_site content">
        <section class="preview_site <?=(!empty($_SESSION['theme']))?$_SESSION['theme']:''?>">
          <article class="preview_site">
            <h2 class="preview_site"><?=(!empty($_SESSION['pseudocode']['titre']))?$_SESSION['pseudocode']['titre']:''?></h2>
            <img class="logo float-left preview_site" src="uploads/<?=(!empty($_SESSION['result']['image_site']))?$_SESSION['result']['image_site']:''?>" alt="Faites du sport">
            <div class="float-right preview_site">
              <?=(!empty($_SESSION['pseudocode']['pseudocode_central']))?$_SESSION['pseudocode']['pseudocode_central']:''?>
            </div>
            <div class="clear"></div>
          </article>
          <!--<article class="preview_site">
            <h2>Lorem Ipsum</h2>
            <p class="preview_site">
              Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
              Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
              Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident,
              sunt in culpa qui officia deserunt mollit anim id est laborum.
            </p>
          </article>
          <article class="preview_site">
            <h2>Lorem Ipsum</h2>
            <p class="preview_site">
              Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
              Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
              Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident,
              sunt in culpa qui officia deserunt mollit anim id est laborum.
            </p>
          </article>-->
        </section>

        <aside class="preview_site <?=(!empty($_SESSION['theme']))?$_SESSION['theme']:''?>">
          <div class="preview_site">
            <?=(!empty($_SESSION['pseudocode']['pseudocode_aside']))?$_SESSION['pseudocode']['pseudocode_aside']:''?>
        </aside>
      </div>

      <div class="clear"></div>
      <footer class="preview_site">
        (c) <?=(!empty($_SESSION['result']['prenom']))?$_SESSION['result']['prenom']:''?> <?=(!empty($_SESSION['result']['nom']))?$_SESSION['result']['nom']:''?> <?=date('Y')?>
      </footer>
    </div>
  </main>
</div>
<?php include_once('./tpl/footer.php') ?>
